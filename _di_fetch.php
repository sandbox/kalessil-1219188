<?php

/*
@author Vladimir Reznichenko <kalessil@gmail.com>
@date 12-Jul-2011
*/

function _di_build_info_url ($project, $branch = '6.x') {
	global $base_url;

	$site_key = md5 ($base_url . drupal_get_private_key ());
	return 'http://updates.drupal.org/release-history/'. $project .'/'. $branch .'?site_key='. $site_key .'&version=';
}

function _di_download ($url, $to) {
	$ch = curl_init ($url);
	$fp = fopen ($to, 'wb');
	curl_setopt ($ch, CURLOPT_FILE, $fp);
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	curl_exec( $ch);
	curl_close ($ch);
	fclose ($fp);
}

function _di_fetch () {
	$data = array ();
	$result = 'ok';
	$projectsFolder = drupal_get_path ('module', 'distrib_installer'). '/../';

	$strError = null;

	$projects = explode (',', $_GET['projects']);
	if (is_array ($projects) && count ($projects)) {
		require_once ('_di_cache_rebuild.php');

		foreach ($projects as $p) {
			$url  = _di_build_info_url ($p);
			$info = @file_get_contents ($url);

			if (empty ($info)) {
				$result   = 'err';
				$strError = t ('Getting project information failed.');
				break;
			}

			$dom = new domDocument ();
			$dom->loadXML ($info);

			$xpath = new DOMXPath ($dom);
			//$query = '//project/releases[1]/release[1]/download_link[1]';
			$query = '//project/releases[1]/release[1]/files[1]/file[2]/url[1]';
			$links = $xpath->query ($query);

			foreach ($links as $l) {
				$url      = $l->textContent;
				$data [$p]= $url;
                                
				if ($url) {
					$file   = substr ($url, strrpos ($url,'/'), strlen ($url));
                                	$target = $_SERVER['DOCUMENT_ROOT'] . file_directory_path () .'/distrib_installer/'. $file;

					//check cache first
					//$query = '//project/releases[1]/release[1]/filesize[1]';
					$query = '//project/releases[1]/release[1]/files[1]/file[2]/size[1]';
					$sizes = $xpath->query ($query);
					$size  = 0;
					foreach ($sizes as $s) {
						$size = $s->textContent;
					}
					if (file_exists ($target) && filesize ($target) == $size) {
						continue;
					}

					//since not cached, let's download it
					_di_download ($url, $target);

					//check if file is ok
					if (!file_exists ($target) || filesize ($target) != $size) {
						unlink ($target);
						$result   = 'error';
						$strError = t ('Archive delivery error.');
						break;
					}

					//now unpack
					$zip = new ZipArchive;
					$res = $zip->open ($target);
					if ($res === TRUE) {
						$zip->extractTo ($projectsFolder);
						$zip->close ();
					} else {
						$result   = 'error';
						$strError = t ('Archive unpacking error.');
						break;
					}

					_di_rebuild_cache ($p);
				}
			}
		}
	}	

	drupal_json (array ('result' => $result, 'message' => $strError, 'data' => $data));
	exit ();
}

?>