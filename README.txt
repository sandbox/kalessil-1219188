
Install this module in modules folder of your site (/sites/all/modules/distrib_install or /sites/your-domain.com/modules/distrib_install).
Follow Side Building -> Modules -> Install Distribution and Install / Enable pack you need.

Module automatically deliver projects into modules folder where distrib_install module located.

When opening Install Distribution page any requirement issues will be displayed.