<?php

/*
@author Vladimir Reznichenko <kalessil@gmail.com>
@date 12-Jul-2011
*/

function _di_project_module (&$projectOrModule) {
	switch ($projectOrModule) {
		//'ckk' project is 'content' module
		//case 'content':
		//	return 'cck';
		case 'cck':
			return 'content';

		default:
			return $projectOrModule;
	}
}

function _di_collect_scripts_info () {
	$scriptsPath = drupal_get_path ('module', 'distrib_installer') . '/scripts';
	$list        = file_scan_directory ($scriptsPath, '\.script$');

	if (!empty ($list)) {
		foreach ($list as $index => $file) {
			//require_once (drupal_get_path ('module', 'distrib_installer') .'/scripts/'. $file->basename);
			$data = drupal_parse_info_file (drupal_get_path ('module', 'distrib_installer') .'/scripts/'. $file->basename);

			foreach ($data['projects'] as $mi => $project) {
				$module = _di_project_module ($project);

				$notAModule = false;
				$path = drupal_get_path ('module', $module);
				if (empty ($path)) { //some modules mismatches project title and drupal recognised module name
					$path = drupal_get_path ('module', str_replace ('_', '', $module));
					if ($path) {
						$module = str_replace ('_', '', $module);
					} else {
						//ok, project not provided anything to be detected as module, it can be a theme
						$path = drupal_get_path ('theme', $module);
						if (empty ($path)) {
							//ok, I don't know what is it, just check is it physycally exists, other not my business
							$possiblePath = drupal_get_path ('module', 'distrib_installer') .'/../'. $module .'/';
							if (file_exists ($possiblePath)) {
								$path = $possiblePath;
							}
						}

						if (!empty ($path)) {
							$notAModule = true;
						}
					}
				}

				$data['projects'][$mi] = array ($project, $path, $notAModule);
				/* if project and module names mismatches, add module name into data array */
				if ($module != $project) {
					$data['projects'][$mi] []= $module;
				}
			}

			$list[$index] = $data;
			unset ($data);
		}
	}
	
	$list['enabled'] = module_list ();

	drupal_json ($list);
	exit ();
}

?>