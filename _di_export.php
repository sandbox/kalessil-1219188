<?php

function _di_export () {
	$listProjects = array ();

	$result = db_query ('SELECT filename, name, info, status FROM {system} WHERE type="module" AND filename LIKE "sites/%"');
	while ($mod = db_fetch_array ($result)) {
		//check if file exists: sometimes system table is not clean, when components non-properly removed 
		if (!file_exists ($mod['filename']) || empty ($mod['status'])) {
			continue;
		}

		$info = unserialize ($mod['info']);
		if (empty ($info['project'])) {
			continue;
		}

		$listProjects [$info['project']] []= $mod['name'];	
	}

	$outputProjects = array ();
	$outputModules  = array ();

	$cntMods = 0;
	foreach ($listProjects as $p => $mods) {
		$outputProjects []= 'projects[]='. $p;

		foreach ($mods as $m ) {
			$outputModules []= 'modules['. $p .'][]='. $m;
			++$cntMods;
		}

		$outputModules []= '';
	}

	$output  = ';'. $_SERVER['HTTP_HOST'] .'@'. date ('Y-m-d H:i:s')."\r\n\r\n";
	$output .= ';'. count ($listProjects) .' projects, '. $cntMods." modules enabled total\r\n";
	$output .= ";Projects listed\r\n\r\n";
	$output .= implode ("\r\n", $outputProjects) ."\r\n\r\n";
	$output .= ";Modules listed\r\n\r\n";
	$output .= implode ("\r\n", $outputModules) ."\r\n";

	unset ($outputProjects, $outputModules);

	return '<pre>'. $output .'</pre>';
}


?>