<?php

/*
@author Vladimir Reznichenko <kalessil@gmail.com>
@date 12-Jul-2011
*/

function _di_enable () {
	$modules = explode (',', $_GET['modules']);

	if (is_array ($modules) && count ($modules)) {
		if ($_GET['rollback']) {
			module_disable ($modules);
		} else {
			module_enable ($modules);
		}
	}

	drupal_json (array ('status' => 'ok', 'list' => $modules, 'rollback' => $_GET['rollback']));
	exit ();
}

?>