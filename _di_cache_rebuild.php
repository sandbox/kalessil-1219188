<?php

/*
@author Vladimir Reznichenko <kalessil@gmail.com>
@date 12-Jul-2011
*/

//see module_rebuild_cache function, it was the prototype
function _di_rebuild_cache ($project) {
	// Get current list of modules

	//file_scan_directory to be used
	$files = drupal_system_listing ('\.module$', 'modules/'. $project, 'name', 0);
	//some project can also not provide module hooks, providing only sources
	if (empty ($files)) {
		return $files; 
	}

	// Extract current files from database.
	system_get_files_database ($files, 'module');
 	ksort ($files);

	// Set defaults for module info
	$defaults = array(
		'dependencies' => array (), 
		'dependents'   => array (), 
		'description'  => '', 
		'version'      => NULL, 
		'php'          => DRUPAL_MINIMUM_PHP,
	);

	foreach ($files as $filename => $file) {
		// Look for the info file.
		$file->info = drupal_parse_info_file (dirname ($file->filename) .'/'. $file->name .'.info');

		// Skip modules that don't provide info.
		if (empty ($file->info)) {
			unset ($files[$filename]);
			continue;
		}
		// Merge in defaults and save.
		$files[$filename]->info = $file->info + $defaults;

		// Invoke hook_system_info_alter() to give installed modules a chance to
		// modify the data in the .info files if necessary. Some modules (cvs_deploy) are claimed to use this hook.
		//drupal_alter('system_info', $files[$filename]->info, $files[$filename]);

		// Log the critical hooks implemented by this module.
		$bootstrap = 0;
		foreach (bootstrap_hooks () as $hook) {
			if (module_hook ($file->name, $hook)) {
        			$bootstrap = 1;
        			break;
			}
		}

		// Update the contents of the system table:
		if (isset ($file->status) || (isset ($file->old_filename) && $file->old_filename != $file->filename)) {
			db_query ("UPDATE {system} SET info = '%s', name = '%s', filename = '%s', bootstrap = %d WHERE filename = '%s'", serialize ($files[$filename]->info), $file->name, $file->filename, $bootstrap, $file->old_filename);
		} else {
			// This is a new module.
			$files[$filename]->status   = 0;
			$files[$filename]->throttle = 0;
      			db_query ("INSERT INTO {system} (name, info, type, filename, status, throttle, bootstrap) VALUES ('%s', '%s', '%s', '%s', %d, %d, %d)", $file->name, serialize ($files[$filename]->info), 'module', $file->filename, 0, 0, $bootstrap);
		}
	}

	return _module_build_dependencies ($files);
}

?>