/*
@author Vladimir Reznichenko <kalessil@gmail.com>
@date 12-Jul-2011
*/

$(document).ready (function () {
	var divHead = $('#distribution-installer-head');
	var divMain = $('#distribution-installer-main');

	divHead.html ('Requesting script list...');

	$.getJSON ('/admin/build/modules/distrib_installer/scripts', {},
		function( data ) {
			divHead.html ('');

			var strHtml = '';
			var sequenceIndex = 0;
			for (var index in data) {
				if (index == 'enabled') {
					continue;
				}

				var script = data[index]['name'];
				var info   = data[index]['info'];
				var list   = [];
				var listEnable  = [];
				var listInstall = [];

				var installed  = true;
				var complected = true;

				for (var moduleIndex in data[index]['projects']) {
					var partially  = false;

					moduleName = data[index]['projects'][moduleIndex][0];
					modulePath = data[index]['projects'][moduleIndex][1];

					if (!modulePath) {
						list.push ('<span class="mod-missing" title="missing" err="0" id="'+ moduleName + "-" + sequenceIndex +'">'+ moduleName + '</span>');
						installed  = false;
						complected = false;
						listInstall.push (moduleName);
					} else {
						if (!data['enabled'][moduleName] 
						    /* project and module names are not always matches */
						    && !(data[index]['projects'][moduleIndex].length == 4 && data['enabled'][  data[index]['projects'][moduleIndex][3]  ])) 
						{
							if (data[index]['projects'][moduleIndex][2]) { //not a module
								list.push ('<span class="mod-unknown" title="exist, but not a module" id="'+ moduleName + "-" + sequenceIndex +'">'+ moduleName +'</span>');
							} else {
								list.push ('<span class="mod-disabled" title="disabled" id="'+ moduleName + "-" + sequenceIndex +'">'+ moduleName +'</span>');
								installed   = false;
								listEnable.push (moduleName);
							}
						} else {
							if (data[index]['modules']) {
								//check if all project modules enabled
								var modList = data[index]['modules'][ moduleName ];
								if (modList) {
									for (var enModIndex in modList) {
										if (!data['enabled'][  modList[enModIndex]  ]) {
											partially = true;
											listEnable.push (modList[enModIndex]);
										}
									}
								}
							}

							if (partially) {
								list.push ('<span class="mod-partial" title="partially enabled" id="'+ moduleName + "-" + sequenceIndex +'">'+ moduleName + '</span>');
								installed = false;
							} else {
								list.push ('<span class="mod-enabled" title="enabled" id="'+ moduleName + "-" + sequenceIndex +'">'+ moduleName + '</span>');
							}
						}
					}					
				}
				var status = (installed ? 'skip' : (complected ? 'enable' : 'install'));

				strHtml += '<div class="row" id="row-'+ sequenceIndex +'" title="'+ script +'"><div class="header">'+ script +', '+ info;
				strHtml += '<div class="controls controls-'+ status +'" num="'+ sequenceIndex +'"enable="'+ listEnable.join(',') +'" install="'+ listInstall.join(',') +'"></div></div><div class="content">';
				strHtml += list.join(', ') + '</div></div>';

				sequenceIndex++;
			}

			divMain.html (strHtml);

			divMain.find ('.controls-enable').each (function () {
				$(this).html('<a href="#" class="button-enable" rollback="" distscript="'+ script +'" enable="'+ $(this).attr('enable') +'">Enable</a>');
			});
			divMain.find ('.controls-install').each (function () {
				$(this).html('<a href="#" class="button-install" distscript="'+ script +'" num="'+ $(this).attr('num') +'" installed="0" install="'+ $(this).attr('install') +'">Install</a>');
			});
			divMain.find ('.controls-skip').each (function () {
				$(this).html('Installed');
			});

			divMain.find ('.button-enable').each (function () {
				$(this).click (function () {
					var link = $(this);
					$.getJSON ('/admin/build/modules/distrib_installer/Xable', 
						{ modules: $(this).attr ('enable'), rollback: $(this).attr ('rollback') },
						function ( data ) {
							if (data['status'] == 'ok') {
								link.html (data['rollback'] ? 'Enable' : 'Done - Undo');
								link.attr ('rollback', (link.attr ('rollback') ? '' : 1));

								if (data['rollback']) {
									link.removeClass ('button-undo');
									
								} else {
									link.addClass ('button-undo');
								}
							}
						}
					);
					return false;
				});
			});

			divMain.find ('.button-install').each (function () {
				$(this).click (function () {
					_di_install_proj_continue (divHead, $(this), true);
					return false;
				})
			});
		}
	);
	
});

function _di_install_proj_continue (divHead, link, started) {
	var projects = link.attr ('install'). split (',');
	if (projects && projects.length) {
		if (started) {
			divHead.html ('Downloading '+ projects.length +' projects: ');
		}

		//walk thru list one by one and install module by module;
		var toInstall  = parseInt (link.attr ('installed'));
		var curProject = 0;
		for (var instProjInd in projects) {
			if (curProject == toInstall) {
				curProject = projects[instProjInd];
				break;
			}

			++curProject;
		}
		//curProject contains project name here

		//TODO: add some animation for project span, telling it's downloading: curProject + link.attr(num)
		$('#'+ curProject +'-'+ link.attr ('num')).addClass ('mod-installing');//css ({'font-weight': 'bold'});

		$.getJSON ('/admin/build/modules/distrib_installer/fetch', 
			{ projects: curProject },
			function ( data ) {
				var modSpan = $('#'+ curProject +'-'+ link.attr ('num'));

				if (data['result'] == 'ok') {
					//TODO: add some animation (first with downloading style)  for project span, telling it's ready
					modSpanId.removeClass ('mod-installing');//css ({'font-weight': 'normal'});

					//report status
					divHead.html (divHead.html () +'+');

					//update counter
					link.attr ('installed', parseInt (link.attr ('installed')) + 1);

					//deside continue or stop
					var il = link.attr ('install'). split (',');
					if (il && parseInt (link.attr ('installed')) >= il.length)  {
						//completed                      
						divHead.html ('Completed.');
						location.reload ();
					} else {
						//continue installation
						_di_install_proj_continue (divHead, link, false);
					}
				} else {
					var retries = parseInt (modSpan.attr ('err'));
					if (retries >= 5) {
						modSpan.attr ('err', retries + 1);
						//continue installation
						_di_install_proj_continue (divHead, link, false);
					} else {
						//TODO: report that trouble installing: report error
						location.reload ();
					}
				}
			}
		);
	}
}