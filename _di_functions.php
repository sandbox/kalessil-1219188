<?php

/*
@author Vladimir Reznichenko <kalessil@gmail.com>
@date 12-Jul-2011
*/

function _di_install_module ($module_name) {
/*
	if installed, then send response

	call installation callback
	send response
*/
}

function _di_check_requirements () {
/*	check update module enabled
        check writable files dir
        create own directory and assign access permission if needed
*/
	$filesPath = $_SERVER['DOCUMENT_ROOT'] . file_directory_path ();
	$strErr    = NULL;
        if (!is_writable ($filesPath)) {
        	$strErr = t ('@folder is not writable. Correct permissions.', array ('@folder' => file_directory_path ()));
	} else {
		$filesPath = $filesPath .'/distrib_installer';
		if (!file_exists ($filesPath) && !mkdir ($filesPath, 0644)) {
			$strErr = t ('@folder is not created. Check permissions.', array ('@folder' => $filesPath));
		} else {
			if (!defined ('UPDATE_DEFAULT_URL')) {
				$strErr = t ('Update module is not enabled.');
			} else {
				if (!function_exists ('curl_init')) {
					$strErr = t ('CURL extention not found.');
				}
				if (!class_exists ('ZipArchive')) {
					$strErr = t ('ZipArchive component not found.');
				}
				$projectsFolder = drupal_get_path ('module', 'distrib_installer'). '/../';
				if (!is_writable ( $projectsFolder )) {
					$strErr = t ('@folder is not writable. Correct permissions.', array ('@folder' => $projectsFolder));
				}
			}
		}
	}

	$result = array (
		'status'  => (is_null ($strErr) ? 'ok' : 'err'),
		'message' => $strErr
	);

	drupal_json ($result);
        exit ();
}

function _di_page () {
	drupal_add_js  (drupal_get_path ('module', 'distrib_installer') . '/distrib_installer.js');
	drupal_add_css (drupal_get_path ('module', 'distrib_installer') . '/distrib_installer.css');
	return '<div id="distribution-installer-head"></div><div id="distribution-installer-main"></div><div id="distribution-installer-foot"></div>';
}

?>